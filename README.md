# Installation

## Manuelle

Installer sur le système les packages suivants (Debian):

 * mysql ou mariadb
 * apache 2
 * php > 8.1
 * php-gd php-xml php-mysql
 * node.js
 * npm
 * composer

Cloner le projet

Exécuter :

```bash
composer update
composer install

npm install
npm run build
```

Créer la BDD

```
php bin/console doctrine:migrations:migrate
```

Exécuter les script SQL sous /datasets/

# Configuration

Définir les variables d'environnemetn suivantes :

_url de connexion au  relai SMTP pour les envoi de mail : MAILER_DSN=smtp://user:pass@smtp.example.com:port_
MAILER_DSN='sendmail://default'

# Créer le premier utilisateur sur un poste de dev

Démmarer le serveur web

Se connecter à https://localhost:8080/register et créer un compte

Se connecter à mysql et exécuter la requête SQL suivante :

```sql
UPDATE user set roles='["ROLE_APP_ADMIN"]' where id = 1;
```
