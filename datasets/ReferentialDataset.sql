INSERT INTO computer_type (id, label, creation_date, updated_date) VALUES (1, 'PC Portable', '2024-06-27 20:24:07', null);
INSERT INTO computer_type (id, label, creation_date, updated_date) VALUES (2, 'PC Fixe', '2024-06-27 20:24:19', null);

INSERT INTO request_status (id, label, creation_date, updated_date, code) VALUES (1, 'Créé', '2024-06-27 18:09:26', null, 'CREATED');
INSERT INTO request_status (id, label, creation_date, updated_date, code) VALUES (2, 'Demandée', '2024-06-27 18:10:20', null, 'SUBMITTED');
INSERT INTO request_status (id, label, creation_date, updated_date, code) VALUES (3, 'Validée', '2024-06-27 18:11:06', null, 'VALIDATED');
INSERT INTO request_status (id, label, creation_date, updated_date, code) VALUES (4, 'En cours', '2024-06-27 18:11:23', null, 'PROVIDING');
INSERT INTO request_status (id, label, creation_date, updated_date, code) VALUES (5, 'Disponible', '2024-06-27 18:12:20', null, 'AVAILABLE');
INSERT INTO request_status (id, label, creation_date, updated_date, code) VALUES (6, 'Clôturée', '2024-06-27 18:12:44', null, 'CLOSED');
