---
tags:dev,environnement
---

Monter un environnement de développement
=======

- Installer php 8 en vérifiant la version minimale dans `composer.json`
- Installer composer
- [facultatif] Installer symfony-cli
