<?php

namespace App\Controller;

use App\Entity\Organization;
use App\Entity\User;
use App\Form\AdminEditUserFormType;
use App\Security\AppRole;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'app_admin')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    #[Route('/admin/users', name: 'app_admin_show_users')]
    public function show_organizations(EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted(AppRole::ROLE_APP_ADMIN);

        $users = $entityManager->getRepository(User::class)->findAll();

        return $this->render('admin/users.html.twig',
        [
            'users' => $users
        ]);
    }

    #[Route('/admin/user/{id}/edit', name: 'app_admin_edit_user')]
    public function edit_user(User $user, Request $request, LoggerInterface $logger, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted(AppRole::ROLE_APP_ADMIN);

        $form = $this->createForm(AdminEditUserFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // TODO gérer la réinitialisation de mot de passe
            // FIXME gérer l'enregistrement des rôles
            $logger->info("Modification de l'utilisateur {userId} (identifiant : {userName}).", [
                'userId' => $user->getId(),
                'userName' => $user->getUsername(),
                'roles' => $user->getRoles()
            ]);
            $user = $form->getData();
            $entityManager->persist($user);
            $entityManager->flush();
        }

        return $this->render('admin/edit_user.html.twig',
            [
                'form' => $form
            ]);
    }

    #[Route('/admin/organization', name: 'app_admin_add_organization')]
    public function add_organization(): Response
    {
        $this->denyAccessUnlessGranted(AppRole::ROLE_APP_ADMIN);

        //$form = $this->createForm(AdminEditUserFormType::class, $user);


        return $this->render('admin/add_organization.html.twig',
            [
                //'form' => $form
            ]);
    }

}


