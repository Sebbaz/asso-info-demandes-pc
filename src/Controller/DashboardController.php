<?php

namespace App\Controller;

use App\Entity\ComputerUser;
use App\Entity\Request;
use App\Entity\RequestStatus;
use App\Entity\User;
use App\Repository\RequestRepository;
use App\Form\NewComputerUserFormType;
use App\Form\NewRequestFormType;
use App\Security\AppRole;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    #[Route(path: '/app', name: 'app_user_dashboard')]
    public function app(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->redirectToRoute('app_account');
    }

    #[Route(path: '/app/new', name: 'app_user_start_new_request')]
    public function newRequest(): Response
    {
        $this->denyAccessUnlessGranted(AppRole::ROLE_APP_REQUESTER);

        return $this->redirectToRoute('app_user_create_computer_user');
    }

    #[Route(path: '/app/request/new', name: 'app_user_create_request')]
    public function request(EntityManagerInterface $entityManager, \Symfony\Component\HttpFoundation\Request $request, LoggerInterface $logger): Response
    {
        $this->denyAccessUnlessGranted(AppRole::ROLE_APP_REQUESTER);

        $computerUser = new ComputerUser();
        $form = $this->createForm(NewComputerUserFormType::class, $computerUser);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $logger->info('Creating computerUser and request.');

            $requestComputer = $entityManager->getRepository(Request::class)->create($this->getUser());

            $computerUser = $form->getData();
            $logger->debug("Adding user " . $computerUser->getName());
            $computerUser->setCreationDate(date_create());
            $entityManager->persist($computerUser);

            $requestComputer->setComputerUser($computerUser);
            $requestComputer->setComputerType($form['computerType']->getData());
            $requestComputer->setCreatedAt(date_create_immutable());
            $requestComputer->setRequestDate(date_create());
            $requestComputer->setStatus($entityManager->getRepository(RequestStatus::class)->getStatusSubmitted());

            $entityManager->persist($requestComputer);
            $entityManager->flush();

            $logger->debug('Persisted');

            return $this->redirectToRoute('app_request_accepted',
                [ 'requestId' => $requestComputer->getId(),]
            );

        }
        return $this->render('computerRequests/newComputerUser.html.twig',
            [
                'computerUserForm' => $form,
            ]
        );
    }

    #[Route(path: '/app/request/{requestId}', name: 'app_request_accepted')]
    public function requestAccepted(int $requestId) : Response {

        $this->denyAccessUnlessGranted(AppRole::ROLE_APP_REQUESTER);

        return $this->render(
            'computerRequests/requestAccepted.html.twig',
            [
                'requestId' => $requestId,
            ]
        );

    }

    #[Route(path: '/app/requests', name: 'app_user_requests')]
    public function showUserRequests(
        EntityManagerInterface $entityManager
    ) : Response {
        $this->denyAccessUnlessGranted(AppRole::ROLE_APP_REQUESTER);

        $user = $this->getUser();
        $requests = $entityManager->getRepository(Request::class)->findByCreatedByUser($user);

        return $this->render('computerRequests/showUserRequests.html.twig',
            [
                'user' => $user,
                'requests' => $requests
            ]
        );
    }
}