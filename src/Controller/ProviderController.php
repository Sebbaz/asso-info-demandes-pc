<?php

namespace App\Controller;

use App\Entity\Request;
use App\Entity\RequestStatus;
use App\Security\AppRole;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProviderController extends AbstractController
{
    #[Route('/provider/requests', name: 'app_provider_requests')]
    public function showRequests(EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted(AppRole::ROLE_APP_PROVIDER);

        $requests = $entityManager->getRepository(Request::class)->findBy(
            [
                'status' => $entityManager->getRepository(RequestStatus::class)->getStatusSubmittedNotClosed()
            ],
            [
                'requestDate' => "DESC"
            ],
            100
        );

        return $this->render('provider/requests.html.twig', [
            'requests' => $requests
        ]);
    }

    #[Route('/provider/request/{id}', name: 'app_provider_edit_request')]
    public function editRequest(Request $request): Response
    {
        return $this->render('provider/edit_request.html.twig', [
            'request' => $request
        ]);
    }

}
