<?php

namespace App\Entity;

use App\Repository\RequestRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RequestRepository::class)]
class Request
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $chargeCriteria = null;

    #[ORM\Column(type: Types::DATETIMETZ_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $lastContactDate = null;

    #[ORM\Column(length: 1000, nullable: true)]
    private ?string $workingNotes = null;

    #[ORM\Column(type: Types::DATETIMETZ_MUTABLE)]
    private ?\DateTimeInterface $requestDate = null;

    #[ORM\ManyToOne(inversedBy: 'requests')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ComputerType $computerType = null;

    #[ORM\ManyToOne(inversedBy: 'requests')]
    #[ORM\JoinColumn(nullable: false)]
    private ?RequestStatus $status = null;

    #[ORM\ManyToOne(inversedBy: 'requests')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ComputerUser $computerUser = null;

    #[ORM\ManyToOne(inversedBy: 'requests')]
    private ?User $CreatedByUser = null;

    #[ORM\Column(type: Types::DATETIMETZ_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $lastUpdated = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRequestOwnerName(): ?string
    {
        return $this->requestOwnerName;
    }

    public function setRequestOwnerName(string $requestOwnerName): static
    {
        $this->requestOwnerName = $requestOwnerName;

        return $this;
    }

    public function getRequestOwnerEmail(): ?string
    {
        return $this->requestOwnerEmail;
    }

    public function setRequestOwnerEmail(?string $requestOwnerEmail): static
    {
        $this->requestOwnerEmail = $requestOwnerEmail;

        return $this;
    }

    public function getChargeCriteria(): ?string
    {
        return $this->chargeCriteria;
    }

    public function setChargeCriteria(?string $chargeCriteria): static
    {
        $this->chargeCriteria = $chargeCriteria;

        return $this;
    }

    public function getLastContactDate(): ?\DateTimeInterface
    {
        return $this->lastContactDate;
    }

    public function setLastContactDate(?\DateTimeInterface $lastContactDate): static
    {
        $this->lastContactDate = $lastContactDate;

        return $this;
    }

    public function getWorkingNotes(): ?string
    {
        return $this->workingNotes;
    }

    public function setWorkingNotes(?string $workingNotes): static
    {
        $this->workingNotes = $workingNotes;

        return $this;
    }

    public function getRequestDate(): ?\DateTimeInterface
    {
        return $this->requestDate;
    }

    public function setRequestDate(\DateTimeInterface $requestDate): static
    {
        $this->requestDate = $requestDate;

        return $this;
    }

    public function getComputerType(): ?ComputerType
    {
        return $this->computerType;
    }

    public function setComputerType(?ComputerType $computerType): static
    {
        $this->computerType = $computerType;

        return $this;
    }

    public function getStatus(): ?RequestStatus
    {
        return $this->status;
    }

    public function setStatus(?RequestStatus $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getComputerUser(): ?ComputerUser
    {
        return $this->computerUser;
    }

    public function setComputerUser(?ComputerUser $computerUser): static
    {
        $this->computerUser = $computerUser;

        return $this;
    }

    public function getIdRequester(): ?Requester
    {
        return $this->idRequester;
    }

    public function setIdRequester(?Requester $idRequester): static
    {
        $this->idRequester = $idRequester;

        return $this;
    }

    public function getCreatedByUser(): ?User
    {
        return $this->CreatedByUser;
    }

    public function setCreatedByUser(?User $CreatedByUser): static
    {
        $this->CreatedByUser = $CreatedByUser;

        return $this;
    }

    public function getLastUpdated(): ?\DateTimeInterface
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated(?\DateTimeInterface $lastUpdated): static
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
