<?php

namespace App\Form;

use App\Entity\Organization;
use App\Entity\User;
use App\Security\AppRole;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminEditUserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username')
            ->add('roles', ChoiceType::class, [
                    'choices'  => [
                        'Fournisseur' => AppRole::ROLE_APP_PROVIDER,
                        'Demandeur' => AppRole::ROLE_APP_REQUESTER,
                        'Admin' => AppRole::ROLE_APP_ADMIN
                    ],
                    'multiple' => true,
                    'required' => false,
                    'empty_data' => '[]'
                ]
            )
            ->add('email', EmailType::class, [
                'required' => false
            ])
            ->add('isVerified')
            ->add('organization', EntityType::class, [
                'class' => Organization::class,
                'choice_label' => 'name',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
