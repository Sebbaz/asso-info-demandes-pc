<?php

namespace App\Form;

use App\Entity\ComputerType;
use App\Entity\ComputerUser;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewComputerUserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Name')
            ->add('Surname')
            ->add('postalCode')
            ->add('City')
            ->add('address')
            ->add('phoneNumber')
            ->add('computerType', EntityType::class, [
                'class' => ComputerType::class,
                'choice_label' => 'label',
                'mapped' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ComputerUser::class,
        ]);
    }
}
