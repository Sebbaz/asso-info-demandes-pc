<?php

namespace App\Repository;

use App\Entity\ComputerType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ComputerType>
 *
 * @method ComputerType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComputerType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComputerType[]    findAll()
 * @method ComputerType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComputerTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComputerType::class);
    }

//    /**
//     * @return ComputerType[] Returns an array of ComputerType objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ComputerType
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
