<?php

namespace App\Repository;

use App\Entity\ComputerUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ComputerUser>
 *
 * @method ComputerUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method ComputerUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method ComputerUser[]    findAll()
 * @method ComputerUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComputerUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ComputerUser::class);
    }

//    /**
//     * @return ComputerUser[] Returns an array of ComputerUser objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ComputerUser
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
