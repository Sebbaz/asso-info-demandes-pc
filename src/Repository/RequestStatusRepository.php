<?php

namespace App\Repository;

use App\Entity\RequestStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RequestStatus>
 *
 * @method RequestStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method RequestStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method RequestStatus[]    findAll()
 * @method RequestStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequestStatusRepository extends ServiceEntityRepository
{

    const CREATED = 'CREATED';
    const SUBMITTED = 'SUBMITTED';
    const VALIDATED = 'VALIDATED';
    const PROVIDING = 'PROVIDING';
    const AVAILABLE = 'AVAILABLE';
    const CLOSED = 'CLOSED';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequestStatus::class);
    }

    public function getStatusSubmitted() : RequestStatus
    {
        return $this->findOneBy([ 'code' => self::SUBMITTED]);
    }

    public function getStatusValidated() : RequestStatus
    {
        return $this->findOneBy([ 'code' => self::VALIDATED]);
    }

    public function getStatusCreated() : RequestStatus
    {
        return $this->findOneBy([ 'code' => self::CREATED]);
    }

    public function getStatusProviding() : RequestStatus
    {
        return $this->findOneBy([ 'code' => self::PROVIDING]);
    }

    public function getStatusAvailable() : RequestStatus
    {
        return $this->findOneBy([ 'code' => self::AVAILABLE]);
    }

    public function getStatusClosed() : RequestStatus
    {
        return $this->findOneBy([ 'code' => self::CLOSED]);
    }

    public function getStatusSubmittedNotClosed() : array
    {
        return $this->findBy([ 'code' => [
            self::SUBMITTED, self::VALIDATED, self::PROVIDING, self::AVAILABLE
            ]]);
    }


//    /**
//     * @return RequestStatus[] Returns an array of RequestStatus objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RequestStatus
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
