<?php

namespace App\Security;

use Psr\Log\LoggerInterface;
use Symfony\Config\TwigExtra\StringConfig;

class AppRole
{

    private String $role;
    private String $label;

    const ROLE_APP_ADMIN = 'ROLE_APP_ADMIN';
    const ROLE_APP_REQUESTER = 'ROLE_APP_REQUESTER';
    const ROLE_APP_PROVIDER = 'ROLE_APP_PROVIDER';

    private array $allowed_roles = [
        self::ROLE_APP_ADMIN,
        self::ROLE_APP_REQUESTER,
        self::ROLE_APP_PROVIDER
    ];

    private array $allowed_roles_labels = [
        self::ROLE_APP_ADMIN => 'Admin',
        self::ROLE_APP_REQUESTER => 'Demandeur',
        self::ROLE_APP_PROVIDER => 'Fournisseur'
        ];

    public function __construct(String $role)
    {
        if(in_array($role, $this->allowed_roles)) {
          $this->role = $role;
          $this->label = $this->allowed_roles_labels[$role];
        }
        else {
            throw new \InvalidArgumentException("You wanted a not well defined role $role. Please check App\Security\AppRole::allowed_roles property");
        }
    }

    public function getRole() : String {
        return $this->role;
    }

    public function getLabel() : String {
        return $this->label;
    }

    public function __toString(): string
    {
        return $this->role;
    }

}